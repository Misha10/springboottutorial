package com.test.security.repository;

import com.test.security.model.Message;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepo extends CrudRepository<Message, Integer> {
    List<Message> findByTag(String tag);

//    @Query("SELECT * FROM abc")
//    List<Message> myCustomQuery(String tag);
}